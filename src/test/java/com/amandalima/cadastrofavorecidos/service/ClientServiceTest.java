package com.amandalima.cadastrofavorecidos.service;

import com.amandalima.cadastrofavorecidos.exception.AccountException;
import com.amandalima.cadastrofavorecidos.exception.ClientException;
import com.amandalima.cadastrofavorecidos.repository.ClientRepository;
import com.amandalima.cadastrofavorecidos.repository.model.Client;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.dao.DataIntegrityViolationException;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ClientServiceTest {

    @Mock
    private ClientRepository repository;

    @InjectMocks
    private ClientService service;

    @Test
    void shouldCreateClient() throws ClientException {

        var expected = new Client(1L, "Client test", 10214563125478L);
        when(repository.save(ArgumentMatchers.any())).thenReturn(expected);

        var result = service.createClient(expected.getCpfCnpj(), expected.getName());

        assertThat(result).usingRecursiveComparison().isEqualTo(expected);
    }

    @Test
    void shouldThrowErrorWhenClientAlreadyExist() throws ClientException {
        when(repository.save(any())).thenThrow(new DataIntegrityViolationException(""));

        assertThatExceptionOfType(ClientException.class)
                .isThrownBy(() -> service.createClient(1L, "Test"))
                .withMessage("Cliente não pode ser cadastrado");
    }

    @Test
    void shouldDeleteClient() {

        when(repository.findFirstByCpfCnpj(any())).thenReturn(Optional.of(Client.builder().build()));
        doNothing().when(repository).delete(any());

        assertDoesNotThrow(() -> service.deleteClient(1234L));
    }

    @Test
    void shouldGetClienttByCpfCnpj() throws AccountException, ClientException {

        var client = new Client(1L, "Client test", 10214563125478L);
        when(repository.findFirstByCpfCnpj(any())).thenReturn(Optional.of(client));

        var result = service.getClientByCpfCnpj(client.getCpfCnpj());

        assertThat(result).usingRecursiveComparison().isEqualTo(client);
    }

    @Test
    void shouldThrowErrorWhenClientIsNotFound() {

        when(repository.findFirstByCpfCnpj(any())).thenReturn(Optional.empty());

        assertThatExceptionOfType(ClientException.class)
                .isThrownBy(() -> service.getClientByCpfCnpj(1234L))
                .withMessage("Cliente não encontrado!");
    }
}