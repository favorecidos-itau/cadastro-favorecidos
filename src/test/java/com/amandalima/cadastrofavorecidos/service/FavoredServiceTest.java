package com.amandalima.cadastrofavorecidos.service;

import com.amandalima.cadastrofavorecidos.exception.AccountException;
import com.amandalima.cadastrofavorecidos.exception.ClientException;
import com.amandalima.cadastrofavorecidos.exception.FavoredException;
import com.amandalima.cadastrofavorecidos.repository.AccountRepository;
import com.amandalima.cadastrofavorecidos.repository.FavoredRepository;
import com.amandalima.cadastrofavorecidos.repository.model.Account;
import com.amandalima.cadastrofavorecidos.repository.model.Client;
import com.amandalima.cadastrofavorecidos.repository.model.Favored;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;

import java.math.BigInteger;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class FavoredServiceTest {

    @Mock
    private ClientService clientService;

    @Mock
    private AccountService accountService;

    @Mock
    private FavoredRepository repository;

    @InjectMocks
    private FavoredService service;

    @Test
    void shouldCreateFavored() throws ClientException, AccountException, FavoredException {

        var client = new Client(1L, "Client test", 10214563125478L);
        var account = new Account(BigInteger.ONE, 100699L, 111L, "Itau", client);
        var expectFavored = Favored.builder().account(account).client(client).build();

        when(clientService.getClientByCpfCnpj(client.getCpfCnpj())).thenReturn(client);
        when(accountService.getAccountById(BigInteger.ONE)).thenReturn(account);
        when(repository.save(any())).thenReturn(expectFavored);

        var result = service.createFavored(client.getCpfCnpj(), account.getId());

        assertThat(result).usingRecursiveComparison().isEqualTo(expectFavored);

    }

    @Test
    void shouldThrowErrorWhenClientNotFound() throws ClientException {

        when(clientService.getClientByCpfCnpj(anyLong()))
                .thenThrow(new ClientException("Cliente não encontrado!", HttpStatus.NOT_FOUND));

        assertThatExceptionOfType(ClientException.class)
                .isThrownBy(() -> service.createFavored(123L, BigInteger.ONE))
                .withMessage("Cliente não encontrado!");
    }

    @Test
    void shouldThrowErrorWhenAccountNotFound() throws AccountException, ClientException {

        var client = new Client(1L, "Client test", 10214563125478L);

        when(clientService.getClientByCpfCnpj(anyLong())).thenReturn(client);
        when(accountService.getAccountById(BigInteger.ONE))
                .thenThrow(new AccountException("Conta não encontrada!", HttpStatus.NOT_FOUND));

        assertThatExceptionOfType(AccountException.class)
                .isThrownBy(() -> service.createFavored(123L, BigInteger.ONE))
                .withMessage("Conta não encontrada!");
    }

    @Test
    void shouldThrowErrorWhenFavoredAlreadyExist() throws ClientException, AccountException {

        var client = new Client(1L, "Client test", 10214563125478L);
        var account = new Account(BigInteger.ONE, 100699L, 111L, "Itau", client);
        when(clientService.getClientByCpfCnpj(client.getCpfCnpj())).thenReturn(client);
        when(accountService.getAccountById(BigInteger.ONE)).thenReturn(account);

        when(repository.save(any())).thenThrow(new DataIntegrityViolationException(""));

        assertThatExceptionOfType(FavoredException.class)
                .isThrownBy(() -> service.createFavored(client.getCpfCnpj(), account.getId()))
                .withMessage("Este favorecido não pode ser registrado.");
    }

    @Test
    void shouldDeleteFavored() {

        when(repository.findById(any())).thenReturn(Optional.of(Favored.builder().build()));
        doNothing().when(repository).delete(any());

        assertDoesNotThrow(() -> service.deleteFavored(BigInteger.ONE));

    }

    @Test
    void shouldGetFavoredById() throws AccountException, FavoredException {

        var client = new Client(1L, "Client test", 10214563125478L);
        var account = new Account(BigInteger.ONE, 100699L, 111L, "Itau", client);
        var expected = Favored.builder().account(account).client(client).build();

        when(repository.findById(any())).thenReturn(Optional.of(expected));

        var result = service.getFavoredById(BigInteger.ONE);

        assertThat(result).usingRecursiveComparison().isEqualTo(expected);
    }
}