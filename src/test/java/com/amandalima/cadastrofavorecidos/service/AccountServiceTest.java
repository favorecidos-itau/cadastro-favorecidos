package com.amandalima.cadastrofavorecidos.service;

import com.amandalima.cadastrofavorecidos.exception.AccountException;
import com.amandalima.cadastrofavorecidos.exception.ClientException;
import com.amandalima.cadastrofavorecidos.repository.AccountRepository;
import com.amandalima.cadastrofavorecidos.repository.model.Account;
import com.amandalima.cadastrofavorecidos.repository.model.Client;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;

import java.math.BigInteger;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class AccountServiceTest {

    @Mock
    private ClientService clientService;

    @Mock
    private AccountRepository repository;

    @InjectMocks
    private AccountService service;

    @Test
    void shouldCreateAccount() throws ClientException, AccountException {

        var client = new Client(1L, "Client test", 10214563125478L);
        var account = new Account(BigInteger.ONE, 100699L, 111L, "Itau", client);

        when(clientService.getClientByCpfCnpj(client.getCpfCnpj())).thenReturn(client);
        when(repository.save(any())).thenReturn(account);

        var result = service.createAccount(client.getCpfCnpj(), account.getAgency(), account.getAccountNumber(), account.getBank());

        assertThat(result).usingRecursiveComparison().isEqualTo(account);

    }

    @Test
    void shouldThrowErrorWhenClientNotFound() throws ClientException {

        when(clientService.getClientByCpfCnpj(anyLong()))
                .thenThrow(new ClientException("Cliente não encontrado!", HttpStatus.NOT_FOUND));

        assertThatExceptionOfType(ClientException.class)
                .isThrownBy(() -> service.createAccount(123L, 123L, 123L, "Itau"))
                .withMessage("Cliente não encontrado!");
    }

    @Test
    void shouldThrowErrorWhenAccountAlreadyExist() throws ClientException {

        var client = new Client(1L, "Client test", 10214563125478L);
        when(clientService.getClientByCpfCnpj(client.getCpfCnpj())).thenReturn(client);

        when(repository.save(any())).thenThrow(new DataIntegrityViolationException(""));

        assertThatExceptionOfType(AccountException.class)
                .isThrownBy(() -> service.createAccount(client.getCpfCnpj(), 123L, 123L, "Itau"))
                .withMessage("Conta não pode ser registrada");
    }

    @Test
    void shouldDeleteAccount() {

        when(repository.findById(any())).thenReturn(Optional.of(Account.builder().build()));
        doNothing().when(repository).delete(any());

        assertDoesNotThrow(() -> service.deleteAccount(BigInteger.ONE));

    }

    @Test
    void shouldGetAccountById() throws AccountException {

        var client = new Client(1L, "Client test", 10214563125478L);
        var expected = new Account(BigInteger.ONE, 100699L, 111L, "Itau", client);

        when(repository.findById(any())).thenReturn(Optional.of(expected));

        var result = service.getAccountById(BigInteger.ONE);

        assertThat(result).usingRecursiveComparison().isEqualTo(expected);
    }
}