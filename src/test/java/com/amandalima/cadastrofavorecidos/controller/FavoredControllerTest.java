package com.amandalima.cadastrofavorecidos.controller;

import com.amandalima.cadastrofavorecidos.dto.FavoredDto;
import com.amandalima.cadastrofavorecidos.exception.AccountException;
import com.amandalima.cadastrofavorecidos.exception.ClientException;
import com.amandalima.cadastrofavorecidos.exception.FavoredException;
import com.amandalima.cadastrofavorecidos.repository.model.Account;
import com.amandalima.cadastrofavorecidos.repository.model.Client;
import com.amandalima.cadastrofavorecidos.repository.model.Favored;
import com.amandalima.cadastrofavorecidos.service.FavoredService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.math.BigInteger;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(FavoredController.class)
@ExtendWith(SpringExtension.class)
class FavoredControllerTest {

    public static final String BASE_URL = "/favoreds";

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private FavoredService service;

    @Test
    void shouldCreateAndReturnFavored() throws Exception {

        var favoredDto = new FavoredDto(100699704923L, BigInteger.ONE );

        var expectedClient = Client.builder()
                .id(1L)
                .cpfCnpj(favoredDto.getCpfCnpj())
                .name("Test Client")
                .build();
        
        var expectedAccount = Account.builder()
                .id(BigInteger.ONE)
                .agency(234L)
                .accountNumber(123L)
                .bank("Itau")
                .client(Client.builder()
                        .id(2L)
                        .cpfCnpj(6584577L)
                        .name("Favored Client")
                        .build())
                .build();
        var expectFavored = Favored.builder().account(expectedAccount).client(expectedClient).build();

        var requestBuilder = MockMvcRequestBuilders.post(BASE_URL)
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(favoredDto));

        when(service.createFavored(favoredDto.getCpfCnpj(), favoredDto.getAccountId()))
                .thenReturn(expectFavored);

        mockMvc.perform(requestBuilder)
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.id").value(expectFavored.getId()))
                .andExpect(jsonPath("$.client").isNotEmpty())
                .andExpect(jsonPath("$.client.id").value(expectedClient.getId()))
                .andExpect(jsonPath("$.client.name").value(expectedClient.getName()))
                .andExpect(jsonPath("$.client.cpfCnpj").value(expectedClient.getCpfCnpj()))
                .andExpect(jsonPath("$.account").isNotEmpty())
                .andExpect(jsonPath("$.account.id").value(expectedAccount.getId()))
                .andExpect(jsonPath("$.account.agency").value(expectedAccount.getAgency()))
                .andExpect(jsonPath("$.account.accountNumber").value(expectedAccount.getAccountNumber()))
                .andExpect(jsonPath("$.account.bank").value(expectedAccount.getBank()))
                .andExpect(jsonPath("$.account.client.id").value(expectedAccount.getClient().getId()))
                .andExpect(jsonPath("$.account.client.name").value(expectedAccount.getClient().getName()))
                .andExpect(jsonPath("$.account.client.cpfCnpj").value(expectedAccount.getClient().getCpfCnpj()));

    }

    @Test
    void shouldThrowErrorWhenFavoredAlreadyExists() throws Exception {

        var favoredDto = new FavoredDto(100699704923L, BigInteger.ONE );

        var requestBuilder = MockMvcRequestBuilders.post(BASE_URL)
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(favoredDto));

        when(service.createFavored(favoredDto.getCpfCnpj(), favoredDto.getAccountId()))
                .thenThrow(new FavoredException("Este favorecido não pode ser registrado.",
                        "Possivelmente este favorecido já esteja cadastrado.",
                        HttpStatus.UNPROCESSABLE_ENTITY));

        mockMvc.perform(requestBuilder)
                .andExpect(status().isUnprocessableEntity())
                .andExpect(jsonPath("$.timestamp").isNotEmpty())
                .andExpect(jsonPath("$.message").value("Este favorecido não pode ser registrado."))
                .andExpect(jsonPath("$.details").value("Possivelmente este favorecido já esteja cadastrado."))
                .andExpect(jsonPath("$.status").value("UNPROCESSABLE_ENTITY"))
                .andExpect(jsonPath("$.statusCode").value("422"))
                .andExpect(jsonPath("$.type").value("FavoredException"))
                .andExpect(jsonPath("$.path").value("uri=" + BASE_URL));

    }

    @Test
    void shouldThrowErrorWhenClientNotFound() throws Exception {

        var favoredDto = new FavoredDto(100699704923L, BigInteger.ONE );

        var requestBuilder = MockMvcRequestBuilders.post(BASE_URL)
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(favoredDto));

        when(service.createFavored(favoredDto.getCpfCnpj(), favoredDto.getAccountId()))
                .thenThrow(new ClientException("Cliente não encontrado!",
                        HttpStatus.NOT_FOUND));

        mockMvc.perform(requestBuilder)
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("$.timestamp").isNotEmpty())
                .andExpect(jsonPath("$.message").value("Cliente não encontrado!"))
                .andExpect(jsonPath("$.details").isEmpty())
                .andExpect(jsonPath("$.status").value("NOT_FOUND"))
                .andExpect(jsonPath("$.statusCode").value("404"))
                .andExpect(jsonPath("$.type").value("ClientException"))
                .andExpect(jsonPath("$.path").value("uri=" + BASE_URL));

    }

    @Test
    void shouldThrowErrorWhenAccountNotFound() throws Exception {

        var favoredDto = new FavoredDto(100699704923L, BigInteger.ONE );

        var requestBuilder = MockMvcRequestBuilders.post(BASE_URL)
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(favoredDto));

        when(service.createFavored(favoredDto.getCpfCnpj(), favoredDto.getAccountId()))
                .thenThrow(new AccountException("Conta não encontrada!", HttpStatus.NOT_FOUND));

        mockMvc.perform(requestBuilder)
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("$.timestamp").isNotEmpty())
                .andExpect(jsonPath("$.message").value("Conta não encontrada!"))
                .andExpect(jsonPath("$.details").isEmpty())
                .andExpect(jsonPath("$.status").value("NOT_FOUND"))
                .andExpect(jsonPath("$.statusCode").value("404"))
                .andExpect(jsonPath("$.type").value("AccountException"))
                .andExpect(jsonPath("$.path").value("uri=" + BASE_URL));

    }

    @Test
    void shouldThrowErrorWhenRequestBodyIsInvalid() throws Exception {

        var requestBuilder = MockMvcRequestBuilders.post(BASE_URL)
                .contentType(MediaType.APPLICATION_JSON);

        mockMvc.perform(requestBuilder)
                .andExpect(status().isBadRequest());
        requestBuilder.content("{\"cpfCnpj\":12345584841}");

        mockMvc.perform(requestBuilder)
                .andExpect(status().isBadRequest());

    }

    @Test
    void shouldDeleteFavored() throws Exception {

        var requestBuilder = MockMvcRequestBuilders.delete(BASE_URL + "/1");

        doNothing().when(service).deleteFavored(BigInteger.ONE);

        mockMvc.perform(requestBuilder)
                .andExpect(status().isOk())
                .andExpect(content().string("Favorecido removido."));
    }

    @Test
    void shouldThrowExceptionWhenFavoredNotFound() throws Exception {

        var requestBuilder = MockMvcRequestBuilders.delete(BASE_URL + "/1");

        doThrow(new FavoredException("Favorecido não encontrado!", HttpStatus.NOT_FOUND))
                .when(service).deleteFavored(BigInteger.ONE);

        mockMvc.perform(requestBuilder)
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("$.timestamp").isNotEmpty())
                .andExpect(jsonPath("$.message").value("Favorecido não encontrado!"))
                .andExpect(jsonPath("$.details").isEmpty())
                .andExpect(jsonPath("$.status").value("NOT_FOUND"))
                .andExpect(jsonPath("$.statusCode").value("404"))
                .andExpect(jsonPath("$.type").value("FavoredException"))
                .andExpect(jsonPath("$.path").value("uri=" + BASE_URL + "/1"));
    }

}