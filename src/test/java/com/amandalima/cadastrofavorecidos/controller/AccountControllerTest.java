package com.amandalima.cadastrofavorecidos.controller;

import com.amandalima.cadastrofavorecidos.dto.AccountDto;
import com.amandalima.cadastrofavorecidos.exception.AccountException;
import com.amandalima.cadastrofavorecidos.exception.ClientException;
import com.amandalima.cadastrofavorecidos.repository.model.Account;
import com.amandalima.cadastrofavorecidos.repository.model.Client;
import com.amandalima.cadastrofavorecidos.service.AccountService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.math.BigInteger;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(AccountController.class)
@ExtendWith(SpringExtension.class)
class AccountControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private AccountService service;

    private static  final String BASE_URL = "/accounts";

    @Test
    void shouldCreateAndRetunrAccount() throws Exception {

        var accountDto = new AccountDto(100699704923L, 111L, 1234L, "Itau");
        var expectedAccount = Account.builder()
                .id(BigInteger.ONE)
                .agency(accountDto.getAgency())
                .accountNumber(accountDto.getAccountNumber())
                .bank(accountDto.getBank())
                .client(Client.builder().id(1L).cpfCnpj(accountDto.getCpfCnpj()).name("Client test").build())
                .build();

        var requestBuilder = MockMvcRequestBuilders.post(BASE_URL)
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(accountDto));

        when(service.createAccount(accountDto.getCpfCnpj(), accountDto.getAgency(), accountDto.getAccountNumber(), accountDto.getBank()))
                .thenReturn(expectedAccount);

        mockMvc.perform(requestBuilder)
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.id").value(expectedAccount.getId()))
                .andExpect(jsonPath("$.agency").value(expectedAccount.getAgency()))
                .andExpect(jsonPath("$.accountNumber").value(expectedAccount.getAccountNumber()))
                .andExpect(jsonPath("$.bank").value(expectedAccount.getBank()))
                .andExpect(jsonPath("$.client").isNotEmpty())
                .andExpect(jsonPath("$.client.id").value(expectedAccount.getClient().getId()))
                .andExpect(jsonPath("$.client.name").value(expectedAccount.getClient().getName()))
                .andExpect(jsonPath("$.client.cpfCnpj").value(expectedAccount.getClient().getCpfCnpj()));
    }

    @Test
    void shouldThrowErrorWhenAccountAlreadyExists() throws Exception {

        var accountDto = new AccountDto(100699704923L, 111L, 1234L, "Itau");

        var requestBuilder = MockMvcRequestBuilders.post(BASE_URL)
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(accountDto));

        when(service.createAccount(accountDto.getCpfCnpj(), accountDto.getAgency(), accountDto.getAccountNumber(), accountDto.getBank()))
                .thenThrow(new AccountException("Conta não pode ser registrada",
                "Possivelmente esta conta já esteja cadastrada.",
                HttpStatus.UNPROCESSABLE_ENTITY));

        mockMvc.perform(requestBuilder)
                .andExpect(status().isUnprocessableEntity())
                .andExpect(jsonPath("$.timestamp").isNotEmpty())
                .andExpect(jsonPath("$.message").value("Conta não pode ser registrada"))
                .andExpect(jsonPath("$.details").value("Possivelmente esta conta já esteja cadastrada."))
                .andExpect(jsonPath("$.status").value("UNPROCESSABLE_ENTITY"))
                .andExpect(jsonPath("$.statusCode").value("422"))
                .andExpect(jsonPath("$.type").value("AccountException"))
                .andExpect(jsonPath("$.path").value("uri=" + BASE_URL));
    }

    @Test
    void shouldThrowErrorWhenClientNotFound() throws Exception {

        var accountDto = new AccountDto(100699704923L, 111L, 1234L, "Itau");

        var requestBuilder = MockMvcRequestBuilders.post(BASE_URL)
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(accountDto));

        when(service.createAccount(accountDto.getCpfCnpj(), accountDto.getAgency(), accountDto.getAccountNumber(), accountDto.getBank()))
                .thenThrow(new ClientException("Cliente não encontrado!", HttpStatus.NOT_FOUND));

        mockMvc.perform(requestBuilder)
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("$.timestamp").isNotEmpty())
                .andExpect(jsonPath("$.message").value("Cliente não encontrado!"))
                .andExpect(jsonPath("$.details").isEmpty())
                .andExpect(jsonPath("$.status").value("NOT_FOUND"))
                .andExpect(jsonPath("$.statusCode").value("404"))
                .andExpect(jsonPath("$.type").value("ClientException"))
                .andExpect(jsonPath("$.path").value("uri=" + BASE_URL));

    }

    @Test
    void shouldThrowErrorWhenRequestBodyIsInvalid() throws Exception {

        var requestBuilder = MockMvcRequestBuilders.post(BASE_URL)
                .contentType(MediaType.APPLICATION_JSON);

        mockMvc.perform(requestBuilder)
                .andExpect(status().isBadRequest());
        requestBuilder.content("{\"agency\":111,\"accountNumber\":1234,\"bank\":\"Itau\"}");

        mockMvc.perform(requestBuilder)
                .andExpect(status().isBadRequest());

    }

    @Test
    void shouldDeleteAccount() throws Exception {

        var requestBuilder = MockMvcRequestBuilders.delete(BASE_URL + "/1");

        doNothing().when(service).deleteAccount(BigInteger.ONE);

        mockMvc.perform(requestBuilder)
                .andExpect(status().isOk())
                .andExpect(content().string("Conta removida."));
    }

    @Test
    void shouldThrowExceptionWhenAccountIsNotFound() throws Exception {

        var requestBuilder = MockMvcRequestBuilders.delete(BASE_URL + "/1");

        doThrow(new AccountException("Conta não encontrada!", HttpStatus.NOT_FOUND))
                .when(service).deleteAccount(BigInteger.ONE);

        mockMvc.perform(requestBuilder)
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("$.timestamp").isNotEmpty())
                .andExpect(jsonPath("$.message").value("Conta não encontrada!"))
                .andExpect(jsonPath("$.details").isEmpty())
                .andExpect(jsonPath("$.status").value("NOT_FOUND"))
                .andExpect(jsonPath("$.statusCode").value("404"))
                .andExpect(jsonPath("$.type").value("AccountException"))
                .andExpect(jsonPath("$.path").value("uri=" + BASE_URL + "/1"));
    }
}