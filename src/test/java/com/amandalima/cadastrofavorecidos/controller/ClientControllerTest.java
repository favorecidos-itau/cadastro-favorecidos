package com.amandalima.cadastrofavorecidos.controller;

import com.amandalima.cadastrofavorecidos.dto.ClientDto;
import com.amandalima.cadastrofavorecidos.exception.ClientException;
import com.amandalima.cadastrofavorecidos.repository.model.Client;
import com.amandalima.cadastrofavorecidos.service.ClientService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(ClientController.class)
@ExtendWith(SpringExtension.class)
class ClientControllerTest {

    public static final String BASE_URL = "/clients";
    
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ClientService service;

    @Test
    void shouldCreateAndReturnClient() throws Exception {

        var clientDto = new ClientDto("Client Test", 100699704923L );
        var expectedClient = Client.builder()
                .id(1L)
                .cpfCnpj(clientDto.getCpfCnpj())
                .name(clientDto.getName())
                .build();

        var requestBuilder = MockMvcRequestBuilders.post(BASE_URL)
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(clientDto));

        when(service.createClient(clientDto.getCpfCnpj(), clientDto.getName()))
                .thenReturn(expectedClient);

        mockMvc.perform(requestBuilder)
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.id").value(expectedClient.getId()))
                .andExpect(jsonPath("$.name").value(expectedClient.getName()))
                .andExpect(jsonPath("$.cpfCnpj").value(expectedClient.getCpfCnpj()));
    }

    @Test
    void shouldThrowErrorWhenClientAlreadyExists() throws Exception {

        var clientDto = new ClientDto("Client Test", 100699704923L );

        var requestBuilder = MockMvcRequestBuilders.post(BASE_URL)
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(clientDto));

        when(service.createClient(clientDto.getCpfCnpj(), clientDto.getName()))
                .thenThrow(new ClientException("Cliente não pode ser cadastrado",
                        "Possivelmente este CPF/CNPJ já esteja cadastrado.",
                        HttpStatus.UNPROCESSABLE_ENTITY));

        mockMvc.perform(requestBuilder)
                .andExpect(status().isUnprocessableEntity())
                .andExpect(jsonPath("$.timestamp").isNotEmpty())
                .andExpect(jsonPath("$.message").value("Cliente não pode ser cadastrado"))
                .andExpect(jsonPath("$.details").value("Possivelmente este CPF/CNPJ já esteja cadastrado."))
                .andExpect(jsonPath("$.status").value("UNPROCESSABLE_ENTITY"))
                .andExpect(jsonPath("$.statusCode").value("422"))
                .andExpect(jsonPath("$.type").value("ClientException"))
                .andExpect(jsonPath("$.path").value("uri=/clients"));

    }

    @Test
    void shouldThrowErrorWhenRequestBodyIsInvalid() throws Exception {

        var requestBuilder = MockMvcRequestBuilders.post(BASE_URL)
                .contentType(MediaType.APPLICATION_JSON);

        mockMvc.perform(requestBuilder)
                .andExpect(status().isBadRequest());
        requestBuilder.content("{\"cpfCnpj\":12345584841}");

        mockMvc.perform(requestBuilder)
                .andExpect(status().isBadRequest());

    }

    @Test
    void shouldDeleteClient() throws Exception {

        var requestBuilder = MockMvcRequestBuilders.delete(BASE_URL + "/1");

        doNothing().when(service).deleteClient(1L);

        mockMvc.perform(requestBuilder)
                .andExpect(status().isOk())
                .andExpect(content().string("Cliente 1 removido."));
    }

    @Test
    void shouldThrowExceptionWhenClientIsNotFound() throws Exception {

        var requestBuilder = MockMvcRequestBuilders.delete(BASE_URL + "/1");

        doThrow(new ClientException("Cliente não encontrado!", HttpStatus.NOT_FOUND))
                .when(service).deleteClient(1L);

        mockMvc.perform(requestBuilder)
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("$.timestamp").isNotEmpty())
                .andExpect(jsonPath("$.message").value("Cliente não encontrado!"))
                .andExpect(jsonPath("$.details").isEmpty())
                .andExpect(jsonPath("$.status").value("NOT_FOUND"))
                .andExpect(jsonPath("$.statusCode").value("404"))
                .andExpect(jsonPath("$.type").value("ClientException"))
                .andExpect(jsonPath("$.path").value("uri=/clients/1"));
    }
}