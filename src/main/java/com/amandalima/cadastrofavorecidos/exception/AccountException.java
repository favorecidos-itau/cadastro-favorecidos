package com.amandalima.cadastrofavorecidos.exception;

import org.springframework.http.HttpStatus;

public class AccountException extends BaseException {

    public AccountException(String message, String details, HttpStatus status) {
        super(message, details, status);
    }

    public AccountException(String message, HttpStatus status) {
        super(message, status);
    }

}
