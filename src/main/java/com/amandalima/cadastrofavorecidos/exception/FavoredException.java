package com.amandalima.cadastrofavorecidos.exception;

import org.springframework.http.HttpStatus;

public class FavoredException extends BaseException {

    public FavoredException(String message, String details, HttpStatus status) {
        super(message, details, status);
    }

    public FavoredException(String message, HttpStatus status) {
        super(message, status);
    }
}
