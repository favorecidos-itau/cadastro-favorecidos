package com.amandalima.cadastrofavorecidos.exception;


import org.springframework.http.HttpStatus;

public class ClientException extends BaseException {


    public ClientException(String message, String details, HttpStatus status) {
        super(message, details, status);
    }

    public ClientException(String message, HttpStatus status) {
        super(message, status);
    }

}
