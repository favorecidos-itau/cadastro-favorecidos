package com.amandalima.cadastrofavorecidos.exception;

import org.springframework.http.HttpStatus;

public class BaseException  extends Exception {

    private final String details;
    private final HttpStatus status;

    public BaseException(String message, String details, HttpStatus status) {
        super(message);
        this.details = details;
        this.status = status;
    }

    public BaseException(String message, HttpStatus status) {
        super(message);
        this.details = null;
        this.status = status;
    }


    public String getDetails() {
        return this.details;
    }

    public HttpStatus getStatus() {
        return this.status;
    }

}
