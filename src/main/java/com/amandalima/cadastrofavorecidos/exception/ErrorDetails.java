package com.amandalima.cadastrofavorecidos.exception;

import lombok.Builder;
import lombok.Data;
import org.springframework.http.HttpStatus;

import java.time.LocalDateTime;

@Data
@Builder
public class ErrorDetails {

    private LocalDateTime timestamp;
    private String message;
    private String details;
    private HttpStatus status;
    private Integer statusCode;
    private String type;
    private String path;
}
