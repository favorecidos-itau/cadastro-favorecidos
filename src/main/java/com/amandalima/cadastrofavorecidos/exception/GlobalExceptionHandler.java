package com.amandalima.cadastrofavorecidos.exception;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.time.LocalDateTime;
import java.util.stream.Collectors;

@ControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler({ClientException.class, AccountException.class, FavoredException.class})
    public ResponseEntity<ErrorDetails> handleBaseException(BaseException ex, WebRequest webRequest) {
        return ResponseEntity.status(ex.getStatus())
                .body(buildError(ex, webRequest));
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
                                                                  HttpHeaders headers, HttpStatus status,
                                                                  WebRequest request) {

        var errorList = ex
                .getBindingResult()
                .getFieldErrors()
                .stream()
                .map(error -> "Campo " + error.getField() + " "+ error.getDefaultMessage())
                .collect(Collectors.toList());
        String details = StringUtils.collectionToDelimitedString(errorList, ",");
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(details);
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<ErrorDetails> handleGenericException(Exception ex, WebRequest request) {
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                .body(buildError(ex, HttpStatus.INTERNAL_SERVER_ERROR, request));
    }

    private ErrorDetails buildError(BaseException ex, WebRequest request) {
        return ErrorDetails.builder()
                .timestamp(LocalDateTime.now())
                .message(ex.getMessage())
                .details(ex.getDetails())
                .status(ex.getStatus())
                .statusCode(ex.getStatus().value())
                .type(ex.getClass().getSimpleName())
                .path(request.getDescription(false))
                .build();
    }

    private ErrorDetails buildError(Exception ex, HttpStatus status, WebRequest request) {
        return ErrorDetails.builder()
                .timestamp(LocalDateTime.now())
                .message(ex.getMessage())
                .status(status)
                .statusCode(status.value())
                .type(ex.getClass().getSimpleName())
                .path(request.getDescription(false))
                .build();
    }

}
