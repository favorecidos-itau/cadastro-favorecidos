package com.amandalima.cadastrofavorecidos.repository;

import com.amandalima.cadastrofavorecidos.repository.model.Account;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.math.BigInteger;

@Repository
public interface AccountRepository extends JpaRepository<Account, BigInteger> {
}
