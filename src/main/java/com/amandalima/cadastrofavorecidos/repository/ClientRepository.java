package com.amandalima.cadastrofavorecidos.repository;

import com.amandalima.cadastrofavorecidos.repository.model.Client;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ClientRepository extends JpaRepository<Client, Long> {


    Optional<Client> findFirstByCpfCnpj(Long cpfCnpj);
}
