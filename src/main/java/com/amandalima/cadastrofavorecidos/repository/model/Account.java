package com.amandalima.cadastrofavorecidos.repository.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigInteger;

@Entity
@Table(name = "conta")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Account implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private BigInteger id;

    @Column(name = "agencia", nullable = false)
    private Long agency;

    @Column(name = "conta", nullable = false)
    private Long accountNumber;

    @Column(name = "banco", nullable = false)
    private String bank;

    @ManyToOne
    @JoinColumn(name = "cliente_cpf_cnpj", referencedColumnName ="cpf_cnpj", nullable = false)
    private Client client;

    public Account(Long agency, Long accountNumber, String bank, Client client) {
        this.agency = agency;
        this.accountNumber = accountNumber;
        this.bank = bank;
        this.client = client;
    }

}
