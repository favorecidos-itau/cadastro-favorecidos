package com.amandalima.cadastrofavorecidos.repository.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "cliente")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Client implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "nome", nullable = false)
    private String name;

    @Column(name = "cpf_cnpj", nullable = false)
    private Long cpfCnpj;

    public Client(Long cpfCnpj, String name) {
        this.name = name;
        this.cpfCnpj = cpfCnpj;
    }

}
