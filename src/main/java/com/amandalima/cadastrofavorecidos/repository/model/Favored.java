package com.amandalima.cadastrofavorecidos.repository.model;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigInteger;

@Entity
@Table(name = "favorecido")
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Favored implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private BigInteger id;

    @ManyToOne
    @JoinColumn(name = "cliente_cpf_cnpj", referencedColumnName ="cpf_cnpj", nullable = false)
    private Client client;

    @ManyToOne
    @JoinColumn(name = "conta_id", referencedColumnName = "id", nullable = false)
    private Account account;

    public Favored(Client client, Account account) {
        this.client = client;
        this.account = account;
    }
}
