package com.amandalima.cadastrofavorecidos.service;

import com.amandalima.cadastrofavorecidos.exception.AccountException;
import com.amandalima.cadastrofavorecidos.exception.ClientException;
import com.amandalima.cadastrofavorecidos.exception.FavoredException;
import com.amandalima.cadastrofavorecidos.repository.FavoredRepository;
import com.amandalima.cadastrofavorecidos.repository.model.Account;
import com.amandalima.cadastrofavorecidos.repository.model.Client;
import com.amandalima.cadastrofavorecidos.repository.model.Favored;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.math.BigInteger;

@Service
public class FavoredService {
    
    @Autowired
    private FavoredRepository repository;

    @Autowired
    private ClientService clientService;

    @Autowired
    private AccountService accountService;

    public Favored createFavored(Long cpfCnpj, BigInteger accountId) throws ClientException, AccountException, FavoredException {

        Client client = clientService.getClientByCpfCnpj(cpfCnpj);
        Account account = accountService.getAccountById(accountId);

        Favored favored = new Favored(client, account);

        try {
            return repository.save(favored);
        } catch (DataIntegrityViolationException e) {
            throw new FavoredException("Este favorecido não pode ser registrado.",
                    "Possivelmente este favorecido já esteja cadastrado.",
                    HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    public void deleteFavored(BigInteger favoredId) throws FavoredException {
        Favored favored = getFavoredById(favoredId);
        repository.delete(favored);
    }

    public Favored getFavoredById(BigInteger favoredId) throws FavoredException {
        return repository.findById(favoredId)
                .orElseThrow(() ->new FavoredException("Favorecido não encontrado!", HttpStatus.NOT_FOUND));
    }
}
