package com.amandalima.cadastrofavorecidos.service;

import com.amandalima.cadastrofavorecidos.exception.AccountException;
import com.amandalima.cadastrofavorecidos.exception.ClientException;
import com.amandalima.cadastrofavorecidos.repository.AccountRepository;
import com.amandalima.cadastrofavorecidos.repository.model.Account;
import com.amandalima.cadastrofavorecidos.repository.model.Client;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.math.BigInteger;

@Service
public class AccountService {

    @Autowired
    private ClientService clientService;

    @Autowired
    private AccountRepository repository;

    public Account createAccount(Long cpfCnpj, Long agency, Long accountNumber, String bank) throws ClientException, AccountException {

        Client client = clientService.getClientByCpfCnpj(cpfCnpj);
        Account account = new Account(agency, accountNumber, bank, client);

        try{
            return repository.save(account);
        } catch (DataIntegrityViolationException e) {
            throw new AccountException("Conta não pode ser registrada",
                    "Possivelmente esta conta já esteja cadastrada.",
                    HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    public void deleteAccount(BigInteger accountId) throws AccountException {
        Account account = getAccountById(accountId);
        repository.delete(account);
    }

    public Account getAccountById(BigInteger accountId) throws AccountException {
        return repository.findById(accountId)
                .orElseThrow(() -> new AccountException("Conta não encontrada!", HttpStatus.NOT_FOUND));
    }
}
