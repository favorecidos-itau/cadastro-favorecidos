package com.amandalima.cadastrofavorecidos.service;

import com.amandalima.cadastrofavorecidos.exception.ClientException;
import com.amandalima.cadastrofavorecidos.repository.ClientRepository;
import com.amandalima.cadastrofavorecidos.repository.model.Client;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Service
public class ClientService {

    @Autowired
    private ClientRepository repository;

    public Client createClient(Long id, String name) throws ClientException {
        Client client = new Client(id, name);
        try {
            return repository.save(client);
        } catch (DataIntegrityViolationException e) {
            throw new ClientException("Cliente não pode ser cadastrado",
                    "Possivelmente este CPF/CNPJ já esteja cadastrado.",
                    HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    public void deleteClient(Long cpfCnpj) throws ClientException {
        var client = getClientByCpfCnpj(cpfCnpj);
        repository.delete(client);
    }

    public Client getClientByCpfCnpj(Long cpfCnpj) throws ClientException {
        return repository.findFirstByCpfCnpj(cpfCnpj)
                .orElseThrow(() -> new ClientException("Cliente não encontrado!", HttpStatus.NOT_FOUND));
    }
}
