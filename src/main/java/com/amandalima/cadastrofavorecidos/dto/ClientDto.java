package com.amandalima.cadastrofavorecidos.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
public class ClientDto {

    @NotNull
    private String name;

    @NotNull
    private Long cpfCnpj;

}
