package com.amandalima.cadastrofavorecidos.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.math.BigInteger;

@Data
@AllArgsConstructor
public class FavoredDto {

    @NotNull
    private Long cpfCnpj;
    @NotNull
    private BigInteger accountId;
}
