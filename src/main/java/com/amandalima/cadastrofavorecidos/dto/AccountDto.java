package com.amandalima.cadastrofavorecidos.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
public class AccountDto {

    @NotNull
    private Long cpfCnpj;
    @NotNull
    private Long agency;
    @NotNull
    private Long accountNumber;
    @NotNull
    private String bank;

}
