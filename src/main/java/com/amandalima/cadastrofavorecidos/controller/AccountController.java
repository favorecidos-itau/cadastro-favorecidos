package com.amandalima.cadastrofavorecidos.controller;

import com.amandalima.cadastrofavorecidos.dto.AccountDto;
import com.amandalima.cadastrofavorecidos.exception.AccountException;
import com.amandalima.cadastrofavorecidos.exception.ClientException;
import com.amandalima.cadastrofavorecidos.repository.model.Account;
import com.amandalima.cadastrofavorecidos.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.math.BigInteger;

@RestController
@RequestMapping("/accounts")
public class AccountController {

    @Autowired
    private AccountService service;

    @PostMapping
    public ResponseEntity<Account> createAccount(@Valid @RequestBody AccountDto dto) throws ClientException, AccountException {
        Account account = service.createAccount(dto.getCpfCnpj(), dto.getAgency(), dto.getAccountNumber(), dto.getBank());
        return ResponseEntity.status(HttpStatus.CREATED).body(account);
    }

    @DeleteMapping("/{accountId}")
    public ResponseEntity<String> deleteAccount(@PathVariable BigInteger accountId) throws AccountException {
        service.deleteAccount(accountId);
        return ResponseEntity.status(HttpStatus.OK).body("Conta removida.");
    }

}
