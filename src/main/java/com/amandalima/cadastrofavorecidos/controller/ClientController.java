package com.amandalima.cadastrofavorecidos.controller;

import com.amandalima.cadastrofavorecidos.dto.ClientDto;
import com.amandalima.cadastrofavorecidos.exception.ClientException;
import com.amandalima.cadastrofavorecidos.repository.model.Client;
import com.amandalima.cadastrofavorecidos.service.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/clients")
public class ClientController {

    @Autowired
    private ClientService service;

    @PostMapping
    public ResponseEntity<Client> createClient(@Valid @RequestBody ClientDto dto) throws ClientException {
        Client client = service.createClient(dto.getCpfCnpj(), dto.getName());
        return ResponseEntity.status(HttpStatus.CREATED).body(client);
    }

    @DeleteMapping(value = "/{cpfCnpj}")
    public ResponseEntity<String> deleteClient(@PathVariable Long cpfCnpj) throws ClientException {
        service.deleteClient(cpfCnpj);
        return ResponseEntity.status(HttpStatus.OK).body(String.format("Cliente %s removido.", cpfCnpj));
    }

}
