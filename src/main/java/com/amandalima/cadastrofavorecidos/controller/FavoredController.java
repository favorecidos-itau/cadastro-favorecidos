package com.amandalima.cadastrofavorecidos.controller;

import com.amandalima.cadastrofavorecidos.dto.FavoredDto;
import com.amandalima.cadastrofavorecidos.exception.AccountException;
import com.amandalima.cadastrofavorecidos.exception.ClientException;
import com.amandalima.cadastrofavorecidos.exception.FavoredException;
import com.amandalima.cadastrofavorecidos.repository.model.Favored;
import com.amandalima.cadastrofavorecidos.service.FavoredService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.math.BigInteger;

@RestController
@RequestMapping("/favoreds")
public class FavoredController {

    @Autowired
    private FavoredService service;

    @PostMapping
    public ResponseEntity<Favored> createFavored(@Valid @RequestBody FavoredDto dto) throws AccountException, ClientException, FavoredException {

        Favored favored = service.createFavored(dto.getCpfCnpj(), dto.getAccountId());
        return ResponseEntity.status(HttpStatus.CREATED).body(favored);
    }

    @DeleteMapping("/{favoredId}")
    public ResponseEntity<String> deleteFavored(@PathVariable BigInteger favoredId) throws FavoredException {
        service.deleteFavored(favoredId);
        return ResponseEntity.ok("Favorecido removido.");
    }

}
