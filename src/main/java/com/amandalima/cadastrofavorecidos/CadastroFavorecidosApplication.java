package com.amandalima.cadastrofavorecidos;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CadastroFavorecidosApplication {

	public static void main(String[] args) {
		SpringApplication.run(CadastroFavorecidosApplication.class, args);
	}

}
